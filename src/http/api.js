/* 
 * 接口统一集成模块
 */
import * as login from './moudules/login'
import * as user from './moudules/user'
import * as dept from './moudules/dept'
import * as role from './moudules/role'
import * as menu from './moudules/menu'
import * as dict from './moudules/dict'
import * as log from './moudules/log'
import * as index from './moudules/index/index'
import * as dyqrgl from './moudules/estate/dyqrgl'
import * as clxxgl from './moudules/estate/clxxgl'
import * as dyywsl from './moudules/estate/dyywsl' 
import * as message from './moudules/estate/message' 
import * as flowable from './moudules/estate/flowable' 

// 默认全部导出
export default {
    login,
    user,
    dept,
    role,
    menu,
    dict,
    log,
    index,
    dyqrgl,
    clxxgl,
    dyywsl,
    message,
    flowable
}