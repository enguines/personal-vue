import axios from '../../axios'

/*
 * 用户管理模块
 */

// 保存
export const save = (data) => {
    return axios({
        url: '/clxx/save',
        method: 'post',
        data
    })
};
// 删除
export const batchDelete = (data) => {
    return axios({
        url: '/clxx/delete',
        method: 'post',
        data
    })
};
// 分页查询
export const findPage = (data) => {
    return axios({
        url: '/clxx/findPage',
        method: 'post',
        data
    })
};

// 查询所有材料信息
export const findAll = (data) => {
    return axios({
        url: '/clxx/findAll',
        method: 'post',
        data
    })
};
