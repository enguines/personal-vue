import axios from '../../axios'

/*
 * 抵押业务受理模块
 */

// 生成业务受理号
export const createYwslh = (data) => {
    return axios({
        url: '/dyywsl/createYwslh',
        method: 'post',
        data
    })
};

// 提交业务
export const submitYwsl = (data) => {
    return axios({
        url: '/dyywsl/submit',
        method: 'post',
        data
    })
};

// 提交抵押权人
export const submitDyqrxx = (data) => {
    return axios({
        url: '/dyywsl/addDyqrs',
        method: 'post',
        data
    })
};

// 提交抵押人
export const submitDyrxx = (data) => {
    return axios({
        url: '/dyywsl/addDyrs',
        method: 'post',
        data
    })
};

// 获取抵押权人信息
export const autofillDyqrxx = (data) => {
    return axios({
        url: '/dyywsl/autofillDyqrxx',
        method: 'post',
        data
    })
}

// 获取受理人信息
export const autofillSlrxx = (data) => {
    return axios({
        url: '/dyywsl/autofillSlrxx',
        method: 'post',
        data
    })
}


/**
 * 获取待办列表
 * @param {*} data
 */
export const queryTodoList = (data) => {
    return axios({
        url: '/dyywsl/queryTodoList',
        method: 'post',
        data
    })
};

/**
 * 认领 / 归还 任务
 * @param {*} data
 */
export const claimBusiness = (data) => {
    return axios({ 
        url: '/dyywsl/claimOrUnclaim',
        method: 'post',
        data
    })
  }


/**
 * 查看业务详情
 * @param {*} data
 */
export const viewBusiness = (ywslh) => {
    return axios({ 
        url: '/dyywsl/viewDetail/'+ ywslh,
        method: 'post',
        ywslh
    })
  }

/**
 * 获取流程图base64
 * @param {*} data 
 */
export const getProcessDiagram = (data) => {
    return axios({ 
        url: '/dyywsl/processDiagram',
        method: 'post',
        data
    })
  }


export const approveBusiness = (data) => {
return axios({ 
    url: '/dyywsl/approveBusiness',
    method: 'post',
    data
})
}
  
