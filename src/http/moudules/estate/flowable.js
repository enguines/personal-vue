import axios from '../../axios'

/*
 * 模板管理
 */

// 获取当前需要处理信息总数
export const queryModel = (data) => {
    return axios({
        url: '/flowable/pageModel',
        method: 'post',
        data
    })
};

// 部署流程
export const deploy = (data) => {
    return axios({
        url: '/flowable/deploy',
        method: 'post',
        data
    })
};
