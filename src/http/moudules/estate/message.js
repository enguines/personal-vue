import axios from '../../axios'

/*
 * 系统消息模块
 */

// 获取当前需要处理信息总数
export const queryMessageAccount = (data) => {
    return axios({
        url: '/message/account',
        method: 'post',
        data
    })
};

