import axios from '../../axios'

// 获取业务受理号
export const createYwslh = (data) => {
    return axios({
        url: '/rest/redis/v1/redisGetAllKeyValue',
        method: 'post',
        data
    })
};
