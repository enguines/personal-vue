import axios from '../../axios'

/*
 * 指数管理模块
 */

// 保存
export const grapIndexDataToday = () => {
    return axios({
        url: '/index/grap',
        method: 'post'
    })
};

/**
 * 将数据导入本地数据库中
 */
export const importData = (data) => {
    return axios({
        url: '/index/import',
        method: 'post',
        data
    })
};

/**
 *  分页查询-指数表现
 */
export const findPageZsbx = (data) => {
    return axios({
        url: '/index/findPageZsbx',
        method: 'post',
        data
    })
};

/**
 *  分页查询-指数估值
 */
export const findPageZsgz = (data) => {
    return axios({
        url: '/index/findPageZsgz',
        method: 'post',
        data
    })
};

/**
 *  分页查询-指数贡献点
 */
export const findPageGxd = (data) => {
    return axios({
        url: '/index/findPageGxd',
        method: 'post',
        data
    })
};

/**
 * 删除指数表现
 * @param {*} data
 */
export const deleteZsbx = (data) => {
    return axios({
        url: '/index/deleteZsbx',
        method: 'post',
        data
    })
};

/**
 * 删除指数估值
 * @param {*} data
 */
export const deleteZsgz = (data) => {
    return axios({
        url: '/index/deleteZsgz',
        method: 'post',
        data
    })
};

/**
 * 删除贡献点
 * @param {*} data
 */
export const deleteGxd = (data) => {
    return axios({
        url: '/index/deleteGxd',
        method: 'post',
        data
    })
};
